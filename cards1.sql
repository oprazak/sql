SELECT card_name, MAX(issued_amount) - MIN(issued_amount) as diff
FROM monthly_cards_issued
GROUP BY card_name
ORDER BY diff DESC
;