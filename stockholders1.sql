SELECT name, SUM(asset)
FROM (
  SELECT stockholders.name, stock, amount * price as asset
  FROM stockholders INNER JOIN stocks ON stockholders.stock = stocks.name AND stockholders.date = stocks.date
) AS subquery
GROUP BY name
;
