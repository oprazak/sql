SELECT pages.page_id
FROM pages LEFT OUTER JOIN page_likes ON pages.page_id = page_likes.page_id
WHERE liked_date IS NULL
ORDER BY pages.page_id ASC
;

SELECT pages.page_id
FROM pages
WHERE pages.page_id NOT IN (SELECT page_id FROM page_likes)
ORDER BY page_id
;