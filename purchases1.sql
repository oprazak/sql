-- Add ranks, then aggregate COUNT over it in superquery

SELECT
  user_id,
  transaction_date,
  COUNT(product_id) AS num_products
FROM (
  SELECT
    user_id,
    transaction_date,
    product_id,
    RANK() OVER(
      PARTITION BY user_id
      ORDER BY transaction_date DESC
    ) AS rank
  FROM user_transactions
) x
WHERE rank = 1
GROUP BY user_id, transaction_date
ORDER BY transaction_date
;