SELECT x.name, SUM(x.total * x.price) AS total_price
FROM (
  SELECT sub.name, sub.total, current.price, current.commodity_name
  FROM (
     -- greatest n per group for stocks
    SELECT s1.name AS commodity_name, s1.date, s1.price
    FROM stocks s1 INNER JOIN (
      -- latest stock prices for each stock
      SELECT name, MAX(date) AS max_date
      FROM stocks
      GROUP BY name
    ) AS s2 ON s1.name = s2.name AND s1.date = s2.max_date
  ) AS current INNER JOIN (
    -- total amount of stocks owned by a holder, per asset
    SELECT stockholders.name, SUM(amount) AS total, stock
    FROM stockholders
    GROUP BY stockholders.name, stock
  ) AS sub ON sub.stock = current.commodity_name
) AS x
GROUP BY name
;