SELECT COUNT(DISTINCT a.company_id)
FROM job_listings a INNER JOIN job_listings b
  ON a.company_id = b.company_id AND a.title = b.title AND a.description = b.description AND a.job_id != b.job_id
;


-- First step is to determine which companies have posted duplicate job listings with the same title and description.
-- We can do so by utilizing the COUNT() aggregate function to calculate the number of job_id entries
-- grouped by company_id, title, and description.
SELECT COUNT(DISTINCT company_id) AS duplicate_companies
FROM (
  SELECT
    company_id,
    title,
    description,
    COUNT(job_id) AS job_count
  FROM job_listings
  GROUP BY company_id, title, description
) AS job_count_cte
WHERE job_count > 1;