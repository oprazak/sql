-- Given a table of Facebook posts, for each user who posted at least twice in 2021,
-- write a query to find the number of days between each user’s first post of the year
-- and last post of the year in the year 2021.
-- Output the user and number of the days between each user's first and last post.

CREATE TABLE posts (
  user_id INTEGER,
  post_id INTEGER,
  post_date TIMESTAMP,
  post_content VARCHAR
);

INSERT INTO posts (user_id, post_id, post_date, post_content) VALUES
  (151652, 599415, '07/10/2021 12:00:00', 'Need a hug'),
  (661093  624356  '07/29/2021 13:00:00', 'Bed.'),
  (004239, 784254, '07/04/2021 11:00:00', 'Happy 4th of July!'),
  (661093, 442560, '07/08/2021 14:00:00'  'Just going'),
  (151652, 111766, '07/12/2021 19:00:00', 'Bye');
