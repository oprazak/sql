-- Write a query to retrieve the top three cities that have the highest number of completed trade orders
-- listed in descending order.
-- Output the city name and the corresponding number of completed trade orders.

CREATE TABLE trades (
  order_id INTEGER,
  user_id INTEGER,
  price DECIMAL,
  quantity INTEGER,
  status VARCHAR,  -- ('Completed' ,'Cancelled')
  timestamp DATETIME
);

INSERT INTO trades (order_id, user_id, price, quantity, status, timestamp) VALUES
  (100101,  111, 9.80,  10, 'Cancelled', '08/17/2022 12:00:00'),
  (100102,  111, 10.00, 10, 'Completed', '08/17/2022 12:00:00'),
  (100259,  148, 5.10,  35, 'Completed', '08/25/2022 12:00:00'),
  (100264,  148, 4.80,  40, 'Completed', '08/26/2022 12:00:00'),
  (100305,  300, 10.00, 15, 'Completed', '09/05/2022 12:00:00'),
  (100400,  178, 9.90,  15, 'Completed', '09/09/2022 12:00:00'),
  (100565,  265, 25.60, 5, 'Completed', '12/19/2022 12:00:00');

CREATE TABLE users (
  user_id INTEGER,
  city VARCHAR,
  email VARCHAR,
  signup_date DATETIME
);

INSERT INTO users (user_id, city, email, signup_date) VALUES
  (111, 'San Francisco', 'rrok10@gmail.com',  '08/03/2021 12:00:00'),
  (148, 'Boston', 'sailor9820@gmail.com',  '08/20/2021 12:00:00'),
  (178, 'San Francisco', 'harrypotterfan182@gmail.co', '01/05/2022 12:00:00'),
  (265, 'Denver', 'shadower_@hotmail.co', '02/26/2022 12:00:00'),
  (300, 'San Francisco' 'houstoncowboy1122@hotmail.com', '06/30/2022 12:00:00');
