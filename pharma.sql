CREATE TABLE pharmacy_sales (
  product_id  integer
  units_sold  integer
  total_sales decimal
  cogs  decimal
  manufacturer  varchar
  drug  varchar
);

INSERT INTO pharmacy_sales (product_id, units_sold, total_sales, cogs, manufacturer, drug) VALUES
  (156, 89514, 3130097.00,  3427421.73,  'Biogen',  'Acyclovir'),
  (25,  222331,  2753546.00,  2974975.36,  'AbbVie',  'Lamivudine and Zidovudine'),
  (50,  90484, 2521023.73,  2742445.90,  'Eli Lilly Dermasorb', 'TA Complete Kit'),
  (98,  110746,  813188.82, 140422.87, 'Biogen',  'Medi-Chord');

-- 1)
-- Each drug is exclusively manufactured by a single manufacturer.
-- Write a query to identify the manufacturers associated with the drugs that resulted in losses
-- and calculate the total amount of losses incurred. 'cogs' is cost of producing the sold goods.
-- Output the manufacturer's name, the number of drugs associated with losses, and the total losses in absolute value.
-- Display the results sorted in descending order with the highest losses displayed at the top.

-- 2)
-- Write a query to calculate the total drug sales for each manufacturer.
-- Round the answer to the nearest million and report your results in descending order of total sales.
-- In case of any duplicates, sort them alphabetically by the manufacturer name.

-- Since this data will be displayed on a dashboard viewed by business stakeholders, please format your results as follows:
-- "$36 million".