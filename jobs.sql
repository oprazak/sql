-- Assume you're given a table containing job postings from various companies on the LinkedIn platform.
-- Write a query to retrieve the count of companies that have posted duplicate job listings.
-- Duplicate job listings are defined as two job listings within the same company that share identical titles and descriptions.

CREATE TABLE job_listings (
  job_id INTEGER,
  company_id INTEGER,
  title VARCHAR,
  description VARCHAR
);

INSERT INTO job_listings (job_id, company_id, title, description) VALUES
(248, 827, 'Business Analyst', 'Business analyst evaluates past and current business data with the primary goal of improving decision-making processes within organizations.'),
(149, 845, 'Business Analyst', 'Business analyst evaluates past and current business data with the primary goal of improving decision-making processes within organizations.'),
(945, 345, 'Data Analyst', 'Data analyst reviews data to identify key insights into a business customers and ways the data can be used to solve problems.'),
(164, 345, 'Data Analyst', 'Data analyst reviews data to identify key insights into a business customers and ways the data can be used to solve problems.'),
(172, 244, 'Data Engineer', 'Data eng');
