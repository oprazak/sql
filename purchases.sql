-- Based on the most recent transaction date,
-- write a query that retrieves the users along with the number of products they bought.
-- Output the user's most recent transaction date, user ID, and the number of products,
-- sorted in chronological order by the transaction date.

CREATE TABLE user_transactions (
  product_id  INTEGER,
  user_id INTEGER,
  spend DECIMAL,
  transaction_date TIMESTAMP
);

INSERT INTO user_transactions (product_id, user_id, spend, transaction_date) VALUES
(3673,  123, 68.90, '07/08/2022 12:00:00'),
(9623,  123, 274.10, '07/08/2022 12:00:00'),
(1467,  115, 19.90, '07/08/2022 12:00:00'),
(2513,  159, 25.00, '07/08/2022 12:00:00'),
(1452,  159, 74.50, '07/10/2022 12:00:00');
