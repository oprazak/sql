SELECT COUNT(message_id) AS msg_count, sender_id
FROM messages
WHERE EXTRACT(MONTH FROM sent_date) = '8'
  AND EXTRACT(YEAR FROM sent_date) = '2022'
GROUP BY sender_id
ORDER BY msg_count DESC
LIMIT 2
;