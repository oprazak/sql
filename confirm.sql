--  You're given tables with information about TikTok user sign-ups and confirmations through email and text.
-- New users on TikTok sign up using their email addresses, and upon sign-up,
-- each user receives a text message confirmation to activate their account.

-- Write a query to display the user IDs of those who did not confirm their sign-up on the first day,
-- but confirmed on the second day.

-- action_date refers to the date when users activated their accounts and confirmed their sign-up through text messages.

CREATE TABLE emails (
  email_id  INTEGER,
  user_id INTEGER,
  signup_date DATETIME
);

INSERT INTO emails (email_id, user_id, signup_date) VALUES
  (125, 7771,  '06/14/2022 00:00:00'),
  (433, 1052,  '07/09/2022 00:00:00');

CREATE TABLE texts (
  ext_id  INTEGER,
  email_id  INTEGER,
  signup_action VARCHAR, -- ('Confirmed', 'Not confirmed')
  action_date DATETIME
);

INSERT INTO texts (ext_id, email_id, signup_action, signup_date) VALUES
  (6878,  125, 'Confirmed', '06/14/2022 00:00:00'),
  (6997,  433, 'Not Confirmed', '07/09/2022 00:00:00'),
  (7000,  433, 'Confirmed', '07/10/2022 00:00:00');
