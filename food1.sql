SELECT name, kind, rk
FROM
  (SELECT
    name,
    kind,
    RANK() OVER (
      PARTITION BY kind
      ORDER BY MAX(price) DESC
    ) AS rk
  FROM foods
  GROUP BY name, kind) sub
WHERE rk < 3
;