SELECT COUNT(order_id) AS trade_count, city
FROM trades t INNER JOIN users u ON t.user_id = u.user_id
WHERE t.status = 'Completed'
GROUP BY city
ORDER BY trade_count DESC
LIMIT 3
;
