-- Write a query to calculate the sum of odd-numbered and even-numbered measurements separately for a particular day
-- and display the results in two different columns. Refer to the Example Output below for the desired format.

-- Within a day, measurements taken at 1st, 3rd, and 5th times are considered odd-numbered measurements,
-- and measurements taken at 2nd, 4th, and 6th times are considered even-numbered measurements.

CREATE TABLE measurements (
  measurement_id  INTEGER,
  measurement_value DECIMAL,
  measurement_time  DATETIME
);


INSERT INTO measurements (measurement_id, measurement_value, measurement_time) VALUES
(131233,  1109.51, '07/10/2022 09:00:00'),
(135211,  1662.74, '07/10/2022 11:00:00'),
(523542,  1246.24, '07/10/2022 13:15:00'),
(143562,  1124.50, '07/11/2022 15:00:00'),
(346462,  1234.14, '07/11/2022 16:45:00');
