-- Assume you're given the table on user viewership categorised by device type
-- where the three types are laptop, tablet, and phone.

-- Write a query that calculates the total viewership for laptops and mobile devices
-- where mobile is defined as the sum of tablet and phone viewership.
-- Output the total viewership for laptops as laptop_reviews and the total viewership for mobile devices as mobile_views.

CREATE TABLE viewships (
  user_id INTEGER,
  device_type VARCHAR,
  view_time TIMESTAMP
);

INSERT INTO viewships (user_id, device_type, view_time) VALUES
  (123, 'tablet', '01/02/2022 00:00:00'),
  (125, 'laptop',  '01/07/2022 00:00:00'),
  (128, 'laptop',  '02/09/2022 00:00:00'),
  (129, 'phone', '02/09/2022 00:00:00'),
  (145, 'tablet',  '02/24/2022 00:00:00');
