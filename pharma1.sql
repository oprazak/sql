SELECT manufacturer, SUM(total_sales - cogs) as loses, COUNT(drug)
FROM pharmacy_sales
WHERE total_sales - cogs < 0
GROUP BY manufacturer
ORDER BY loses DESC
;
