-- Write a query to obtain a breakdown of the time spent sending vs. opening snaps
-- as a percentage of total time spent on these activities grouped by age group.
-- Round the percentage to 2 decimal places in the output.

-- Calculate the following percentages:
--     time spent sending / (Time spent sending + Time spent opening)
--     Time spent opening / (Time spent sending + Time spent opening)
-- To avoid integer division in percentages, multiply by 100.0 and not 100.

CREATE TABLE activities (
  activity_id INTEGER,
  user_id INTEGER,
  activity_type VARCHAR, -- ('send', 'open', 'chat')
  time_spent DOUBLE PRECISION,
  activity_date DATETIME,
);

INSERT INTO activities (activity_id, user_id, activity_type, time_spent, activity_date) VALUES
  (7274, 123, 'open',  4.50,  '06/22/2022 12:00:00'),
  (2425, 123, 'send',  3.50,  '06/22/2022 12:00:00'),
  (1413, 456, 'send',  5.67,  '06/23/2022 12:00:00'),
  (1414, 789, 'chat',  11.00, '06/25/2022 12:00:00'),
  (2536  456, 'open', 3.00,  '06/25/2022 12:00:00');

CREATE TABLE age_breakdown (
  user_id INTEGER,
  age_bucket VARCHAR  --('21-25', '26-30', '31-25')
);

INSERT INTO age_breakdown (user_id, age_bucket) VALUES
  (123,  '31-35'),
  (456, '26-30'),
  (789, '21-25');

