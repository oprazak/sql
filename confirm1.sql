
--  Does this really work though? What if someone signs up at 8:00 but confirms at 16:00 on the following day?
SELECT DISTINCT user_id
FROM emails
INNER JOIN texts
  ON emails.email_id = texts.email_id
WHERE texts.action_date = emails.signup_date + INTERVAL '1 day'
  AND texts.signup_action = 'Confirmed';


SELECT user_id
FROM emails INNER JOIN texts ON emails.email_id = texts.email_id
WHERE signup_date::DATE + 1 = action_date::DATE AND signup_action = 'Confirmed'
;