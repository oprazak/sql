CREATE TABLE stockholders (
  id SERIAL PRIMARY KEY,
  name VARCHAR(20) NOT NULL,
  date DATE NOT NULL,
  stock VARCHAR(20) NOT NULL,
  amount INTEGER NOT NULL
);

CREATE TABLE stocks (
  id SERIAL PRIMARY KEY,
  name VARCHAR(20) NOT NULL,
  date DATE NOT NULL,
  price INTEGER NOT NULL
);

INSERT INTO stockholders (name, date, stock, amount) VALUES
  ('Rick', '2024-04-04', 'cocoa', 5),
  ('Morty', '2024-04-04', 'bananas', 10),
  ('Rick', '2024-04-05', 'oranges', 5);

INSERT INTO stocks (name, date, price) VALUES
  ('cocoa', '2024-04-04', 5),
  ('bananas', '2024-04-04', 10),
  ('oranges', '2024-04-04', 20),
  ('cocoa', '2024-04-05', 1),
  ('bananas', '2024-04-05', 5),
  ('oranges', '2024-04-05', 10);
