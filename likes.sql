-- Assume you're given two tables containing data about Facebook Pages and their respective likes
-- (as in "Like a Facebook Page").

-- Write a query to return the IDs of the Facebook pages that have zero likes.
-- The output should be sorted in ascending order based on the page IDs.

CREATE TABLE pages (
  page_id INTEGER,
  page_name VARCHAR
);

CREATE TABLE page_likes (
  user_id INTEGER,
  page_id INTEGER,
  liked_date DATE
);

INSERT INTO pages (page_id, page_name) VALUES
  (20001,  'SQL Solutions'),
  (20002,  'Rocket Rockstarts'),
  (20045, 'Brain Exercises'),
  (20701, 'Tips for Data Analysts');

INSERT INTO page_likes (user_id, page_id, liked_date) VALUES
  (111, 20001, '2022-08-04'),
  (121, 20045, '2022-12-03'),
  (156, 20001, '2022-06-25');
