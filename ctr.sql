-- Write a query to calculate the click-through rate (CTR) for the app in 2022 and round the results to 2 decimal places.
-- Percentage of click-through rate (CTR) = 100.0 * Number of clicks / Number of impressions
-- To avoid integer division, multiply the CTR by 100.0, not 100.

CREATE TABLE events (
  app_id  INTEGER
  event_type  STRING
  timestamp DATETIME
);

INSERT INTO events (app_id, event_type, timestamp) VALUES
  (123, 'impression',  '07/18/2022 11:36:12'),
  (123, 'impression',  '07/18/2022 11:37:12'),
  (123, 'click', '07/18/2022 11:37:42'),
  (234, 'impression',  '07/18/2022 14:15:12'),
  (234, 'click', '07/18/2022 14:16:12');
