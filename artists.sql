-- Write a query to find the top 5 artists whose songs appear most frequently in the Top 10 of the global_song_rank table.
-- Display the top 5 artist names in ascending order, along with their song appearance ranking.

-- If two or more artists have the same number of song appearances, they should be assigned the same ranking,
-- and the rank numbers should be continuous (i.e. 1, 2, 2, 3, 4, 5)

CREATE TABLE artists (
  artist_id INTEGER,
  artist_name VARCHAR,
  label_owner VARCHAR
);

INSERT INTO artists (artist_id, artist_name, label_owner) VALUES
  (101, 'Ed Sheeran',  'Warner Music Group').
  (120, 'Drake', 'Warner Music Group'),
  (125, 'Bad Bunny', 'Rimas Entertainment');

CREATE TABLE songs (
  song_id INTEGER,
  artist_id INTEGER,
  name  VARCHAR,
);

INSERT INTO songs (song_id, artist_id, name) VALUES
  (55511, 101, 'Perfect'),
  (45202, 101, 'Shape of You'),
  (22222, 120, 'One Dance'),
  (19960, 120, 'Hotline Bling');

CREATE TABLE global_song_rang (
  day INTEGER, -- (1-52)
  song_id INTEGER,
  rank  INTEGER, -- (1-1,000,000)
);

INSERT INTO global_song_rang (day, song_id, rank) VALUES
  (1, 45202, 5),
  (3, 45202, 2),
  (1, 19960, 3),
  (9, 19960, 15);

