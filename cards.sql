CREATE TABLE monthly_cards_issued (
  issue_month integer
  issue_year  integer
  card_name string
  issued_amount integer
);

INSERT INTO monthly_cards_issued (issue_month, issue_year, card_name, issued_amount) VALUES
('Chase Freedom Flex', 55000, 1, 2021),
('Chase Freedom Flex',  60000, 2, 2021),
('Chase Freedom Flex',  65000, 3, 2021),
('Chase Freedom Flex',  70000, 4, 2021),
('Chase Sapphire Reserve',  170000,  1, 2021),
('Chase Sapphire Reserve',  175000,  2, 2021),
('Chase Sapphire Reserve',  180000,  3, 2021);

-- 1)
-- Write a query that outputs the name of each credit card and the difference in the number of issued cards
-- between the month with the highest issuance cards and the lowest issuance.
-- Arrange the results based on the largest disparity.

-- 2)
-- Write a query that outputs the name of the credit card, and how many cards were issued in its launch month.
-- The launch month is the earliest record in the monthly_cards_issued table for a given card.
-- Order the results starting from the biggest issued amount.
