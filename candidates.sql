-- Given a table of candidates and their skills, you're tasked with finding the candidates best suited for an open Data Science job.
-- You want to find candidates who are proficient in Python, Tableau, and PostgreSQL.
-- Write a query to list the candidates who possess all of the required skills for the job.
-- Sort the output by candidate ID in ascending order.

CREATE TABLE candidates (
  candidate_id INTEGER,
  skill VARCHAR
);

INSERT INTO candidates (skill) VALUES
  (1, 'Python'),
  (2, 'Python'),
  (2, 'Tableau'),
  (2, 'PostgreSQL'),
  (3, 'PostgreSQL'),
  (4, 'PostgreSQL'),
  (4, 'R'),
  (4, 'Q'),
  (5, 'Python'),
  (5, 'PostgreSQL'),
  (5, 'Tableau'),
;