CREATE TABLE foods (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  kind VARCHAR  NOT NULL,
  price INTEGER NOT NULL
);

INSERT INTO foods (name, kind, price) VALUES
  ('banana', 'fruit', 3),
  ('mango', 'fruit', 20),
  ('kiwi', 'fruit', 5),
  ('milk', 'dairy', 12),
  ('yoghurt', 'dairy', 2),
  ('tvaroh', 'dairy', 6),
  ('bread', 'pastry', 8),
  ('cupcake', 'pastry', 4),
  ('baguette', 'pastry', 7);
