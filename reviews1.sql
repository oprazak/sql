SELECT
  EXTRACT(MONTH FROM submit_date) AS mth,
  product_id,
  ROUND(AVG(stars), 2) AS avg_stars
FROM reviews
GROUP BY
  EXTRACT(MONTH FROM submit_date),
  product_id
ORDER BY mth, product_id;

SELECT ROUND(AVG(stars), 2) AS avg_stars, product_id, EXTRACT(MONTH from submit_date) AS mth
FROM review
GROUP BY product_id, mth
ORDER BY mth, product_id DESC
;