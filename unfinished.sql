-- Tesla is investigating production bottlenecks and they need your help to extract the relevant data.
-- Write a query to determine which parts have begun the assembly process but are not yet finished.

-- Assumptions:
--     parts table contains all parts currently in production, each at varying stages of the assembly process.
--     An unfinished part is one that lacks a finish_date

CREATE TABLE parts (
  part VARCHAR,
  finish_date DATETIME,
  step INTEGER
);

INSERT INTO parts (part, finish_date, step) VALUES
  ('battery', '01/22/2022 00:00:00', 1),
  ('battery', '03/22/2022 00:00:00', 2),
  ('battery', '01/22/2022 00:00:00', 3),
  ('bumper', '01/22/2022 00:00:00', 1),
  ('bumper', '01/22/2022 00:00:00', 2),
  ('bumper', NULL, 3),
  ('bumper', NULL, 4);
