-- Write a query to obtain a histogram of tweets posted per user in 2022. Output the tweet count per user as the bucket and the number of Twitter users who fall into that bucket.
-- In other words, group the users by the number of tweets they posted in 2022 and count the number of users in each group.

-- tweet_bucket  users_num
-- 1             2
-- 2             1

CREATE TABLE tweets (
  tweet_id SERIAL PRIMARY KEY,
  user_id INTEGER,
  msg VARCHAR,
  tweet_date DATE
);

INSERT INTO tweets (user_id, msg, tweet_date) VALUES
  (111, '', '2021-08-05'),
  (111, '', '2022-08-05'),
  (111, '', '2023-08-05'),

  (112, '', '2022-09-05'),
  (112, '', '2022-10-05'),

  (113, '', '2022-11-05'),

  (114, '', '2023-08-05'),
  (114, '', '2023-08-05');
