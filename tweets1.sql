SELECT COUNT(tweet_num) AS tweet_bucket, tweet_num AS users_num
FROM (
  SELECT COUNT(tweet_id) AS tweet_num, user_id
  FROM tweets
  WHERE EXTRACT(year FROM tweet_date) = '2022'
  GROUP BY user_id
) sub
GROUP BY tweet_num
;

SELECT
  tweet_count_per_user AS tweet_bucket,
  COUNT(user_id) AS users_num
FROM (
  SELECT
    user_id,
    COUNT(tweet_id) AS tweet_count_per_user
  FROM tweets
  WHERE tweet_date BETWEEN '2022-01-01'
    AND '2022-12-31'
  GROUP BY user_id) AS total_tweets
GROUP BY tweet_count_per_user;