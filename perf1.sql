SELECT
  date,
  close,
  LEAD(close) OVER (ORDER BY date) AS next_month_close,
  LAG(close) OVER (ORDER BY date) AS prev_month_close
FROM stock_prices
WHERE EXTRACT(YEAR FROM date) = 2023
  AND ticker = 'GOOG';

SELECT
  date,
  close,
  LEAD(close) OVER (ORDER BY date) AS next_month_close,
  LEAD(close) OVER (ORDER BY date) - close as difference
FROM stock_prices
WHERE EXTRACT(YEAR FROM date) = 2023
  AND ticker = 'GOOG';

SELECT
  date,
  close,
  LAG(close, 3) OVER (ORDER BY date) AS three_months_ago_close,
  close - LAG(close, 3) OVER (ORDER BY date) AS difference
FROM stock_prices
WHERE EXTRACT(YEAR FROM date) = 2023
  AND ticker = 'GOOG';
