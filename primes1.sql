-- Step 1: Calculate the total square footage and the number of items for both prime and non-prime batches

-- To start, we summarize the relevant information by calculating the total square footage
-- and the number of items for each item_type (prime and non-prime).

SELECT
  item_type,
  SUM(square_footage) AS total_sqft,
  COUNT(*) AS item_count
FROM inventory
GROUP BY item_type;

-- This query returns the total square footage and item count for prime and non-prime items.
-- item_type total_sqft  item_count
-- prime_eligible  555.20  6
-- not_prime 128.50  4
-- Step 2: Calculate the maximum number of prime items

-- Next, we calculate the maximum number of prime batches based on the total square footage of a single batch of prime items. Here's a formula that explains how many times it can be accommodated in the warehouse:

-- Maximum Number of Prime Items Batches = FLOOR(Warehouse Area / Total Prime Area)

-- Maximum Number of Prime Items = FLOOR((Warehouse Area / Total Prime Area) x Number of Prime Batches)

WITH summary AS (
  SELECT
    item_type,
    SUM(square_footage) AS total_sqft,
    COUNT(*) AS item_count
  FROM inventory
  GROUP BY item_type
)

SELECT
  item_type,
  total_sqft,
  FLOOR(500000/total_sqft) AS prime_item_batch_count,
  (FLOOR(500000/total_sqft) * item_count) AS prime_item_count
FROM summary
WHERE item_type = 'prime_eligible';

-- item_type total_sqft  prime_item_combination_count  prime_item_count
-- prime_eligible  555.20  900 5400

-- The result shows that the warehouse can stock 900 batches of prime items. Each batch of prime items occupies an area of 555.20 sq ft. Therefore, the total area occupied by prime items is 900 * 555.20 sq ft = 499,680 sq ft.

-- To calculate the remaining warehouse space available for non-prime items, we subtract the total area occupied by prime items from the warehouse's total area: 500,000 sq ft - 499,680 sq ft = 320 sq ft.
-- Step 3: Calculate the maximum number of non-prime items that can be stored

-- Now, let's calculate the maximum number of non-prime batches that can be stored in the warehouse.

-- (1) Allocating Remaining Warehouse Space:

-- We start by calculating the remaining warehouse space after storing the maximum number of prime batches.

-- Remaining Warehouse Space = Total Warehouse Area - Total Area Occupied by Prime Items

-- (2) Calculate the Maximum Number of Non-Prime Items:

-- With the remaining warehouse space determined, we calculate the maximum number of non-prime batches that can be stored.

-- Maximum Number of Non-Prime batches = FLOOR((Remaining Warehouse Space / Area per Non-Prime batch) * Number of Non-Prime batches per Area)

-- Based on the findings:

--     Remaining warehouse space = 320 sq ft.
--     Area per non-prime batch= 128.50 sq ft.
--     Number of non-prime batch per sq ft = 4 items

-- Maximum Number of Non-Prime Batches = FLOOR((320 sq ft / 128.50 sq ft) x 4 items) = 8 non-prime batches

-- Here is the final query to accomplish this:

WITH summary AS (
  SELECT
    item_type,
    SUM(square_footage) AS total_sqft,
    COUNT(*) AS item_count
  FROM inventory
  GROUP BY item_type
),
prime_occupied_area AS (
  SELECT
    item_type,
    total_sqft,
    FLOOR(500000/total_sqft) AS prime_item_batch_count,
    (FLOOR(500000/total_sqft) * item_count) AS prime_item_count
  FROM summary
  WHERE item_type = 'prime_eligible'
)

SELECT
  item_type,
  CASE
    WHEN item_type = 'prime_eligible'
      THEN (FLOOR(500000/total_sqft) * item_count)
    WHEN item_type = 'not_prime'
      THEN FLOOR((500000 - (SELECT FLOOR(500000/total_sqft) * total_sqft FROM prime_occupied_area)) / total_sqft) * item_count
  END AS item_count
FROM summary
ORDER BY item_type DESC;

-- Final results:
-- item_type item_count
-- prime_eligible  5400
-- not_prime 8

-- Method #2: Using FILTER and UNION ALL operator

WITH summary AS (
  SELECT
    SUM(square_footage) FILTER (WHERE item_type = 'prime_eligible') AS prime_sq_ft,
    COUNT(item_id) FILTER (WHERE item_type = 'prime_eligible') AS prime_item_count,
    SUM(square_footage) FILTER (WHERE item_type = 'not_prime') AS not_prime_sq_ft,
    COUNT(item_id) FILTER (WHERE item_type = 'not_prime') AS not_prime_item_count
  FROM inventory
),
prime_occupied_area AS (
  SELECT FLOOR(500000/prime_sq_ft)*prime_sq_ft AS max_prime_area
  FROM summary
)

SELECT
  'prime_eligible' AS item_type,
  FLOOR(500000/prime_sq_ft)*prime_item_count AS item_count
FROM summary

UNION ALL

SELECT
  'not_prime' AS item_type,
  FLOOR((500000-(SELECT max_prime_area FROM prime_occupied_area))
    / not_prime_sq_ft) * not_prime_item_count AS item_count
FROM summary;