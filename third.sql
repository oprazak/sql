-- Write a query to obtain the third transaction of every user. Output the user id, spend and transaction date.

CREATE TABLE transactions (
  user_id INTEGER,
  spend DECIMAL,
  transaction_date TIMESTAMP
);

INSERT INTO transactions (user_id, spend, transaction_date) VALUES
  (111, 100.50  01/08/2022 12:00:00),
  (111, 55.00 01/10/2022 12:00:00),
  (121, 36.00 01/18/2022 12:00:00),
  (145, 24.99 01/26/2022 12:00:00),
  (111, 89.60 02/05/2022 12:00:00);
