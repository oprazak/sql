-- Calculate the difference in closing prices between consecutive months.
-- Calculate the difference between the closing price of the current month and the closing price from 3 months prior.

CREATE TABLE stock_prices (
  date  DATETIME, -- The specific date and time of the stock data.
  ticker  VARCHAR, -- Indicates the stock ticker symbol (e.g., GOOG) for the corresponding company.
  open  DECIMAL(5,2), -- The opening price of the stock on that particular date.
  high  DECIMAL(5,2), -- The highest price reached by the stock during the day.
  low DECIMAL(5,2), -- The lowest price reached by the stock during the day.
  close DECIMAL(5,2)
);

INSERT INTO stock_prices (date, ticker, open, high, low, close) VALUES
  ('08/01/2023 00:00:00', 'GOOG',  130.85,  132.92,  127.78,  130.17),
  ('07/01/2023 00:00:00', 'GOOG',  120.32,  134.07,  115.83,  133.11),
  ('06/01/2023 00:00:00', 'GOOG',  123.50,  129.55,  116.91,  120.97),
  ('05/01/2023 00:00:00', 'GOOG',  107.72,  127.05,  104.50,  123.37),
  ('04/01/2023 00:00:00', 'GOOG',  102.67,  109.63,  102.38,  108.22);
