-- Write a query to calculate the year-on-year growth rate for the total spend of each product,
-- grouping the results by product ID.

-- The output should include the year in ascending order, product ID, current year's spend, previous year's spend
-- and year-on-year growth percentage, rounded to 2 decimal places.

CREATE TABLE user_transactions (
  transaction_id  INTEGER,
  product_id  INTEGER,
  spend DECIMAL,
  transaction_date  DATETIME,
);

INSERT INTO user_transactions (transaction_id, product_id, spend, transaction_date) VALUES
  (1341,  123424,  1500.60, '12/31/2019 12:00:00'),
  (1423,  123424,  1000.20, '12/31/2020 12:00:00'),
  (1623,  123424,  1246.44, '12/31/2021 12:00:00'),
  (1322,  123424,  2145.32, '12/31/2022 12:00:00');

